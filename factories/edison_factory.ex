defmodule EdisonFactory do
  use ExMachina.Ecto, repo: Edison.Repo

  def user_factory do
    %Edison.Users.User{
      name: sequence(:name, &"User ##{&1}"),
      authentication_token: sequence(:token, &"==#{&1}-token-#{&1}=="),
      todoist_key: nil
    }
  end

  def task_factory do
    %Edison.Todo.Task{
      name: sequence(:task_name, &"Task ##{&1}"),
      external_id: sequence(:task_external_id, &"#{&1}"),
      source: "internal",
      user: build(:user)
    }
  end
end

defmodule Adapters.Internal do
  @moduledoc """
  Mocked adapter treating Tasks from Database as external source of tasks
  Simplified just to run some testing without real adapter
  """

  @doc """
  Method to get active tasks from Todoist
  """
  def get_active_tasks(user_id) do
    tasks =
      user_id
      |> Edison.Todo.list_tasks("internal")
      |> Enum.map(fn task ->
        %Adapters.ExternalTask{
          external_id: task.external_id,
          source: "internal",
          raw: nil,
          name: task.name
        }
      end)

    {:ok, tasks}
  end

  @doc """
  Method to return updated version of task
  """
  def update_task_name(user_id, external_id, new_name) do
    %{user_id: ^user_id} = task = Edison.Todo.get_task_by_external_id(external_id)

    {:ok,
     %Adapters.ExternalTask{
       external_id: task.external_id,
       source: "internal",
       raw: nil,
       name: new_name
     }}
  end
end

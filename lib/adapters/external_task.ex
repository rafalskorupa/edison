defmodule Adapters.ExternalTask do
  @moduledoc """
  Struct that unifies all tasks formats to our main application friendly.
  """
  defstruct [:external_id, :name, :source, :raw]
end

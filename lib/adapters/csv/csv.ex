defmodule Adapters.CSV do
  @moduledoc """
  Simplified adapter to parse CSV files
  """

  @doc """
  Method to get active tasks from Todoist
  """
  def get_active_tasks(file) do
    csv_stream(file)
    |> Enum.map(&wrap_external_task/1)
  end

  @doc """
  Method to persist external changes and return updated version of task 
  """
  def update_task_name(_file, _task_id, _new_name) do
    {:error, :not_implemented}
  end

  @doc """
  Method takes parsed csv row with only two columns and wraps it to common %ExternalTask{}

  The raw version is a map due to the fact I haven't predicted adapters that aren't using something map-like
  Actually this adater is not in specification of (recruitment) task, 
  so I left it as it is, but it should be improved
  """
  def wrap_external_task([id, name]) do
    %Adapters.ExternalTask{
      external_id: id,
      name: name,
      source: "csv",
      raw: %{row: "#{id},#{name}"}
    }
  end

  defp csv_stream(file) do
    file
    |> File.stream!()
    |> CSV.decode!()
  end
end

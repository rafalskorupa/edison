defmodule Adapters.Todoist.Tasks do
  @moduledoc """
  Module for handling Task-related requests
  https://developer.todoist.com/rest/v1/#tasks
  """

  @doc """
  Get active tasks
  """
  def get_active_tasks(todoist_client) do
    todoist_client
    |> Tesla.get("/v1/tasks")
    |> case do
      {:ok, %{body: tasks, status: 200}} ->
        tasks
        |> Enum.map(&simplify_todoist_task/1)

      anything ->
        {:error, anything}
    end
  end

  @doc """
  Get active task by external_id
  """
  def get_active_task(todoist_client, external_id) do
    todoist_client
    |> Tesla.get("/v1/tasks/#{external_id}")
    |> case do
      {:ok, %{body: task, status: 200}} ->
        {:ok, simplify_todoist_task(task)}

      anything ->
        {:error, anything}
    end
  end

  @doc """
  Update task  with given external_id
  https://developer.todoist.com/rest/v1/#update-a-task
  Currently supporting only update with name
  """
  def update_task(todoist_client, external_id, %{name: content}) do
    todoist_client
    |> Tesla.post("/v1/tasks/#{external_id}", %{content: content})
    |> case do
      {:ok, %{body: "", status: 204}} -> get_active_task(todoist_client, external_id)
      {:ok, %{status: 404}} -> {:error, :not_found}
      anything -> {:error, anything}
    end
  end

  @doc """
  This method maps response tasks common adapters ExternalTask struct
  """
  def simplify_todoist_task(%{"id" => external_id, "content" => name} = raw) do
    %Adapters.ExternalTask{
      external_id: external_id,
      name: name,
      raw: raw,
      source: "todoist"
    }
  end
end

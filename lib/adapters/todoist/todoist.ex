defmodule Adapters.Todoist do
  @moduledoc """
  Module used for communication with Todoist API.
  """
  @todoist_api "https://api.todoist.com/rest"

  alias Adapters.Todoist.Tasks
  alias Tesla.Middleware.{BaseUrl, Headers, JSON}

  @doc """
  Method to get active tasks from Todoist
  """
  def get_active_tasks(api_token) do
    client(api_token)
    |> Tasks.get_active_tasks()
  end

  @doc """
  Method to update task's name on Todoist and return latest version of task
  """
  def update_task_name(api_token, external_id, new_name) do
    client(api_token)
    |> Tasks.update_task(external_id, %{name: new_name})
  end

  @doc """
  Tesla Client with runtime-generated AuthorizationHeader
  """
  def client(api_token) do
    middleware = [
      {BaseUrl, @todoist_api},
      JSON,
      {Headers, [{"authorization", "Bearer " <> api_token}]}
    ]

    Tesla.client(middleware)
  end
end

defmodule Adapters.AdapterHelpers do
  @moduledoc """
  Module for miscelanous methods helping accessing adapters
  """

  @doc """
  Method returns proper adapter module basing on string representation of it.
  """
  def adapter("todoist"), do: Adapters.Todoist
  def adapter("internal"), do: Adapters.Internal
  def adapter("csv"), do: Adapters.CSV

  def adapter(adapter_name), do: raise(ArgumentError, "Invalid adapter name: #{adapter_name}")
end

defmodule Edison.Support.TokenGenerator do
  @moduledoc false

  import Base, only: [url_encode64: 1]

  def generate_token do
    :crypto.strong_rand_bytes(64) |> url_encode64
  end
end

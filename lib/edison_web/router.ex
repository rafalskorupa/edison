defmodule EdisonWeb.Router do
  use EdisonWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :json_api do
    plug :accepts, ["json", "multipart"]
  end

  pipeline :restricted_json_api do
    plug :json_api
    plug EdisonWeb.Plugs.Authentication
  end

  scope "/", EdisonWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/admin", EdisonWeb.Admin, as: :admin do
    pipe_through :browser

    resources "/users", UserController
  end

  scope "/api", EdisonWeb.Api do
    pipe_through :restricted_json_api

    scope "/v1", V1 do
      get "/sessions/current", SessionController, :current
      post "/:source/sync", TaskController, :sync

      resources "/tasks", TaskController, only: [:index, :show, :update]
    end
  end
end

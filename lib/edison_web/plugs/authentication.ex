defmodule EdisonWeb.Plugs.Authentication do
  @moduledoc """
  Responsible for request authorization. 

  In case of valid authentication_token the user is added to conn.assings.
  Otherwise plug halts the connection and return json response with appropriate status.
  """
  import Plug.Conn

  import Edison.Users, only: [get_by_authentication_token: 1]

  def init(default), do: default

  def call(conn, _default) do
    conn
    |> get_req_header("authorization")
    |> user_from_token()
    |> case do
      {:ok, user} -> assign_current_user(conn, user)
      {:error, error_reason} -> halt_with_error(conn, error_reason)
      _ -> halt_with_error(conn, :undefined)
    end
  end

  defp assign_current_user(conn, user) do
    conn |> assign(:current_user, user)
  end

  defp halt_with_error(conn, :missing_token) do
    return_error_response(conn, 401, "Authorization Token missing")
  end

  defp halt_with_error(conn, :invalid_token) do
    return_error_response(conn, 400, "Authorization  Token invalid")
  end

  defp halt_with_error(conn, _) do
    return_error_response(conn, 500, "Internal Server Error")
  end

  defp user_from_token([token]) do
    case get_by_authentication_token(token) do
      nil -> {:error, :invalid_token}
      user -> {:ok, user}
    end
  end

  defp user_from_token(_), do: {:error, :missing_token}

  defp return_error_response(conn, status, error_message) do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(status, Jason.encode!(%{error: error_message}))
    |> halt()
  end
end

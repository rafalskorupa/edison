defmodule EdisonWeb.Api.V1.TaskView do
  use EdisonWeb, :view
  alias EdisonWeb.Api.V1.TaskView

  def render("sync.json", %{sync: data}) do
    %{
      data: %{
        created: render_many(Map.get(data, :task_insert, []), TaskView, "task.json"),
        updated: render_many(Map.get(data, :task_update, []), TaskView, "task.json"),
        deleted: render_many(Map.get(data, :task_delete, []), TaskView, "task.json")
      }
    }
  end

  def render("index.json", %{tasks: tasks}) do
    %{data: render_many(tasks, TaskView, "task.json")}
  end

  def render("show.json", %{task: task}) do
    %{data: render_one(task, TaskView, "task.json")}
  end

  def render("task.json", %{task: task}) do
    %{id: task.id, name: task.name, source: task.source, external_id: task.external_id}
  end
end

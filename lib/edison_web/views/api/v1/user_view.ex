defmodule EdisonWeb.Api.V1.SessionView do
  use EdisonWeb, :view

  def render("user.json", %{user: user}) do
    %{
      data: %{
        id: user.id,
        name: user.name
      }
    }
  end
end

defmodule EdisonWeb.Api.V1.TaskController do
  use EdisonWeb, :controller

  alias Edison.Todo
  alias Edison.Todo.Task

  action_fallback EdisonWeb.FallbackController

  def index(conn, params) do
    with {:ok, tasks} <- Todo.all_tasks_by(current_user(conn).id, params) do
      render(conn, "index.json", tasks: tasks)
    end
  end

  def sync(conn, %{"source" => "csv", "file" => file}) do
    with {:ok, data} <-
           Todo.synchronise_with_source(
             current_user(conn).id,
             {"csv", file.path}
           ),
         do: render(conn, "sync.json", sync: data)
  end

  def sync(conn, %{"source" => source}) do
    with {:ok, data} <-
           Todo.synchronise_with_source(
             current_user(conn).id,
             source
           ),
         do: render(conn, "sync.json", sync: data)
  end

  def show(conn, %{"id" => id}) do
    task = get_task!(conn, id)

    render(conn, "show.json", task: task)
  end

  def update(conn, %{"id" => id, "task" => %{"name" => name}}) do
    task = get_task!(conn, id)

    with {:ok, %Task{} = task} <- Todo.update_task(task, %{name: name}) do
      render(conn, "show.json", task: task)
    end
  end

  def delete(conn, %{"id" => id}) do
    task = get_task!(conn, id)

    with {:ok, %Task{}} <- Todo.delete_task(task) do
      send_resp(conn, :no_content, "")
    end
  end

  defp current_user(conn) do
    conn.assigns.current_user
  end

  def get_task!(conn, id) do
    Todo.get_task!(id, %{user_id: current_user(conn).id})
  end
end

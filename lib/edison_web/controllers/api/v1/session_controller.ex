defmodule EdisonWeb.Api.V1.SessionController do
  use EdisonWeb, :controller

  def current(conn, _params) do
    render(conn, "user.json", user: conn.assigns.current_user)
  end
end

defmodule EdisonWeb.PageController do
  use EdisonWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end

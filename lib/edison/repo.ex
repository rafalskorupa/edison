defmodule Edison.Repo do
  use Ecto.Repo,
    otp_app: :edison,
    adapter: Ecto.Adapters.Postgres
end

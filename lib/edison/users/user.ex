defmodule Edison.Users.User do
  @moduledoc """
  The User Schema
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias Edison.Support.TokenGenerator

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "users" do
    field :authentication_token, :string
    field :name, :string
    field :todoist_key, :string

    timestamps()
  end

  @doc """
  Changeset for creating new user

  There is slight possibility that generated authentication_token will be already generated in database, 
  such scenario is not handled and it should be investigated whether it's possible to occur 
  and how to handle it.
  """
  def create_changeset(attributes) do
    %__MODULE__{}
    |> cast(attributes, [:name, :todoist_key])
    |> put_new_authentication_token()
    |> validate_changeset()
  end

  @doc false
  def changeset(user, attributes) do
    user
    |> cast(attributes, [:name, :todoist_key])
    |> validate_changeset()
  end

  defp validate_changeset(%Ecto.Changeset{} = changeset) do
    changeset
    |> validate_required([:name, :authentication_token])
    |> unique_constraint(:name)
    |> unique_constraint(:authentication_token)
  end

  defp put_new_authentication_token(%Ecto.Changeset{} = changeset) do
    put_change(changeset, :authentication_token, TokenGenerator.generate_token())
  end
end

defmodule Edison.Todo do
  @moduledoc """
  The Todo context.
  """

  import Ecto.Query, warn: false
  alias Edison.Repo

  alias Edison.Todo.Task

  alias Edison.Todo.Tasks.Synchronise
  alias Edison.Todo.Tasks.UpdateTaskName
  import Edison.Todo.Tasks.TaskFilter, only: [filter_by: 2]

  @doc """
  Synchronise tasks for user with given_id with appropriate source.
  """
  def synchronise_with_source(user_id, source) do
    Synchronise.synchronise_with_source(user_id, source)
  end

  @doc """
  Returns the list of tasks for given user_id

  ## Examples

      iex> list_tasks(user_id)
      [%Task{}, ...]

  """
  def list_tasks(user_id, source \\ nil) do
    Task
    |> by_user_id(user_id)
    |> by_source(source)
    |> Repo.all()
  end

  @doc """
  Returns all tasks for user_id filtered by filter_params
  """
  def all_tasks_by(user_id, %{} = params) do
    Task
    |> by_user_id(user_id)
    |> filter_by(params)
    |> case do
      {:ok, query} -> {:ok, Repo.all(query)}
      error -> error
    end
  end

  @doc """
  Gets a single task.

  Raises `Ecto.NoResultsError` if the Task does not exist.

  ## Examples

      iex> get_task!(123)
      %Task{}

      iex> get_task!(456)
      ** (Ecto.NoResultsError)

  """
  def get_task!(id), do: Repo.get!(Task, id)
  def get_task!(id, %{user_id: user_id}), do: Repo.get!(by_user_id(Task, user_id), id)

  @doc """
  Gets a single task by external id
  """
  def get_task_by_external_id(external_id), do: Repo.get_by(Task, external_id: external_id)

  @doc """
  Creates a task.

  ## Examples

      iex> create_task(%{field: value})
      {:ok, %Task{}}

      iex> create_task(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_task(attrs \\ %{}) do
    %Task{}
    |> Task.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a task.

  ## Examples

      iex> update_task(task_id, %{name: name})
      {:ok, %Task{id: task_id}}

      iex> update_task(task_id, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_task(task, %{name: name}) do
    UpdateTaskName.call(task, name)
  end

  @doc """
  Deletes a Task.

  ## Examples

      iex> delete_task(task)
      {:ok, %Task{}}

      iex> delete_task(task)
      {:error, %Ecto.Changeset{}}

  """
  def delete_task(%Task{} = task) do
    Repo.delete(task)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking task changes.

  ## Examples

      iex> change_task(task)
      %Ecto.Changeset{source: %Task{}}

  """
  def change_task(%Task{} = task) do
    Task.changeset(task, %{})
  end

  defp by_user_id(relation, user_id) do
    from task in relation,
      where: task.user_id == ^user_id
  end

  defp by_source(relation, nil), do: relation

  defp by_source(relation, source) do
    from task in relation,
      where: task.source == ^source
  end
end

defmodule Edison.Integrations do
  @moduledoc """
  The Integration context.

  Provides methods to access integration 
  and for further improvement it should contain logic to add/modify/delete user integrations
  """

  alias Edison.Integrations.Integration
  alias Edison.Users.User
  import Edison.Users, only: [get_user!: 1]

  @doc """
  Returns Integration struct for given user and integration name
  """
  def get_integration(user_id, {"csv", file}) do
    %Integration{
      name: "csv",
      adapter: "csv",
      env: file,
      user_id: user_id
    }
  end

  def get_integration(user_id, "todoist") do
    case get_user!(user_id) do
      %User{todoist_key: nil} ->
        {:error, :not_found}

      %User{todoist_key: todoist_key} ->
        %Integration{
          adapter: "todoist",
          env: todoist_key,
          name: "todoist",
          user_id: user_id
        }
    end
  end

  def get_integration(user_id, "internal") do
    %Integration{
      adapter: "internal",
      env: get_user!(user_id).id,
      name: "internal",
      user_id: user_id
    }
  end

  def get_integration(_, integration), do: raise("invalid integration: #{integration}")
end

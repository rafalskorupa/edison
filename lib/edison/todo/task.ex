defmodule Edison.Todo.Task do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  @allowed_attributes [:name, :source, :external_id, :raw, :user_id]
  @required_attributes [:name, :source, :external_id, :user_id]

  schema "tasks" do
    field :name, :string
    field :source, :string
    field :external_id, :string
    field :raw, :map

    belongs_to(:user, Edison.Users.User)

    timestamps()
  end

  @doc false
  def changeset(task, attrs) do
    task
    |> cast(attrs, @allowed_attributes)
    |> validate_required(@required_attributes)
    |> assoc_constraint(:user)
  end

  @doc """
  """
  def update_name_changeset(task, name), do: changeset(task, %{name: name})

  @doc """
  """
  def new_changeset_from_external(%Adapters.ExternalTask{} = ext, user_id) do
    changeset(%__MODULE__{}, %{
      name: ext.name,
      source: ext.source,
      external_id: "#{ext.external_id}",
      raw: ext.raw,
      user_id: user_id
    })
  end

  def update_changeset_from_external(task, %Adapters.ExternalTask{} = ext) do
    changeset(task, %{
      name: ext.name,
      raw: ext.raw
    })
  end
end

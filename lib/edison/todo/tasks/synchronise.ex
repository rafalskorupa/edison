defmodule Edison.Todo.Tasks.Synchronise do
  @moduledoc """
  Contains logic related to Synchronising task from source.

  Actually, it's quite complicated and it could be simplified
  """

  alias Ecto.Multi
  alias Edison.Repo

  alias Edison.Integrations.Integration

  import Edison.Todo, only: [all_tasks_by: 2]
  import Edison.Integrations, only: [get_integration: 2]
  import Adapters.AdapterHelpers, only: [adapter: 1]

  import Edison.Todo.Task,
    only: [
      new_changeset_from_external: 2,
      update_changeset_from_external: 2
    ]

  @doc """
  Synchronise all tasks from source for user with given_id

  It fetches new tasks from source and from database, then it's comparing
  old collection with new collection and map differences to appropriate changeset
  that are applied to database in single DB transaction.

  In case of real system and production-grade solution, the best way to do that would be probably
  CQRS/async approach - user fires up command to synchronise and wait for data asynchronously.
  """
  def synchronise_with_source(user_id, source) do
    with %Integration{} = integration <- get_integration(user_id, source),
         active_tasks <- fetch_active_tasks!(integration),
         {:ok, local_tasks} <- all_tasks_by(user_id, %{source: integration.name}) do
      local_tasks
      |> pair_by_external_id(active_tasks)
      |> build_changeset_multi(Multi.new(), user_id)
      |> Repo.transaction()
      |> result()
    end
  end

  @doc """
  Fetches active_tasks from integration
  """
  def fetch_active_tasks!(%Integration{name: name, env: env}) do
    adapter(name).get_active_tasks(env)
  end

  @doc """
  This method enumerate over task_pairs {task_from_database, task_from_source}
  and reduce them into multi using add_change/3 method. 
  add_change/3 is responsible for comparing the pair 
  and decide what should be changed in our local database
  """
  def build_changeset_multi(task_pairs, multi, user_id) do
    Enum.reduce(task_pairs, multi, fn {old_task, new_task}, multi ->
      add_change(multi, old_task, new_task, user_id)
    end)
  end

  @doc """
  Okay, this method is quite huge and ugly, but it works.

  1. It creates a 'dictionaries' - maps with {task.external_id, task} values
  2. It get all external_ids from both collections and return union collection
  3. It enumerate over unique external_ids and 'zip' old_task with new_task 
  with appropriate external_ids - if one of the tasks from the pair is missing is fine,
  then one element of the tuple is going to have a nil value.
  """
  def pair_by_external_id(old_tasks, new_tasks) do
    old_tasks_dictionary = dictionary_by_external_id(old_tasks)
    new_tasks_dictionary = dictionary_by_external_id(new_tasks)

    {old_tasks_dictionary, new_tasks_dictionary}
    |> unique_dictionary_keys()
    |> Enum.map(fn external_id ->
      {
        Map.get(old_tasks_dictionary, external_id),
        Map.get(new_tasks_dictionary, external_id)
      }
    end)
  end

  @doc """
  Method get two maps 'dictionaries' and return MapSet with all keys from either dictionary
  """
  def unique_dictionary_keys({dict1, dict2}) do
    MapSet.union(
      dictionary_keys_set(dict1),
      dictionary_keys_set(dict2)
    )
  end

  def dictionary_keys_set(dictionary), do: dictionary |> Map.keys() |> MapSet.new()

  def dictionary_by_external_id(tasks),
    do: Map.new(tasks, fn task -> {"#{task.external_id}", task} end)

  @doc """
  Maps multi results into user-friendly map. Actually it should be moved to representer,
  because it's not so-related to business logic, but I kept it there, because I didn't want 
  to spend on this too much time.
  """
  def result({:ok, results} = _multi) do
    {:ok,
     Enum.group_by(
       results,
       fn {{type, _}, _} -> type end,
       fn {{_, _}, task} -> task end
     )}
  end

  def result(error), do: {:error, error}

  @doc """
  Method compares old_task and new_task and decide to what action data should be forwarded 
  """
  def add_change(multi, task, nil, _user_id), do: delete_task(multi, task)

  def add_change(multi, nil, task, user_id), do: create_task(multi, task, user_id)

  def add_change(multi, old_task, new_task, _user_id),
    do: update_task(multi, old_task, new_task)

  def delete_task(multi, task), do: Multi.delete(multi, {:task_delete, task.external_id}, task)

  @doc """
  Add changeset to insert new task to multi
  """
  def create_task(multi, task, user_id) do
    Multi.insert(
      multi,
      {:task_insert, task.external_id},
      new_changeset_from_external(task, user_id)
    )
  end

  @doc """
  Tricky and not obvious - adds changeset to update local task to multi.
  If there is no change in task's name it does nothing. It's not the perfect solution,
  as in future we may be interested in more data from task, but for now it's fine.

  The most appropriate field would be probably :raw field, but I'm not sure whether its 
  current implementation and representation in database is optimal for this comparison.
  """
  def update_task(multi, %{name: name}, %{name: name}), do: multi

  def update_task(multi, old_task, new_task) do
    Multi.update(
      multi,
      {:task_update, old_task.external_id},
      update_changeset_from_external(old_task, new_task)
    )
  end
end

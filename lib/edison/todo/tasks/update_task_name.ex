defmodule Edison.Todo.Tasks.UpdateTaskName do
  @moduledoc false

  alias Edison.Repo
  alias Edison.{Integrations, Integrations.Integration, Todo.Task}
  alias Edison.Todo.Task

  import Adapters.AdapterHelpers

  @doc """
  Updates given task with new name
  """
  def call(%Task{} = task, name) do
    case Task.update_name_changeset(task, name) do
      %{valid?: true} -> update_task(task, name)
      changeset -> {:error, changeset}
    end
  end

  @doc """
  Perform a update request to source and then updates it in database.

  Actually there is an  edge cased that I decided to ignore:

  If database update fails we don't rollback changes on source 

  Actually is not so-bad, because our tasks will synchronise with a normal
  synchronisation request.
  However, we could ignore this problem by updating task in local database
  and then apply changes to external source, and wrap the whole operation in transaction,
  but I wanted to keep all task's data in our local database up-to-date and in this 
  scenario we have to update task's raw field after request.

  In case of real system and production-grade solution, the best way to do that would be probably
  CQRS/async approach - user fires up command to update and wait for data asynchronously.
  """
  def update_task(task, name) do
    with {:ok, external_task} <- update_external_task_name(task, name),
         {:ok, task} <- update_local_task(task, external_task),
         do: {:ok, task}
  end

  def update_external_task_name(
        %Task{user_id: user_id, source: source, external_id: external_id},
        name
      ) do
    %Integration{} = integration = Integrations.get_integration(user_id, source)

    adapter(integration.adapter).update_task_name(integration.env, external_id, name)
  end

  def update_local_task(task, external_task) do
    Task.update_changeset_from_external(task, external_task) |> Repo.update()
  end
end

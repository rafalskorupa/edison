defmodule Edison.Todo.Tasks.TaskFilter do
  @moduledoc """
  Module responsible for filtering Tasks
  """

  import Ecto.Changeset
  use Ecto.Schema

  import Ecto.Query, warn: false

  @primary_key false

  embedded_schema do
    field :name, :string
    field :source, :string
  end

  def filter_by(relation, filter_params) do
    case params(filter_params) do
      %{valid?: false} = changeset -> {:error, changeset}
      %{valid?: true} = changeset -> {:ok, apply_filters(relation, apply_changes(changeset))}
    end
  end

  defp params(%{} = filter_params) do
    %__MODULE__{}
    |> cast(filter_params, [:name, :source])
  end

  defp apply_filters(relation, %__MODULE__{} = filters) do
    relation
    |> by_name(filters.name)
    |> by_source(filters.source)
  end

  defp by_name(relation, nil), do: relation

  defp by_name(relation, name) do
    from task in relation, where: ilike(task.name, ^name)
  end

  defp by_source(relation, nil), do: relation

  defp by_source(relation, source) do
    from task in relation,
      where: task.source == ^source
  end
end

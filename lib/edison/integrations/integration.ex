defmodule Edison.Integrations.Integration do
  @moduledoc """
  Struct for keeping integration's data consistent between adapters / sources used

  I didn't want to make it over-engineered, but in real-case scenario it should be stored in database,
  to 'emulate' it and make it easier to happen in the future struct defines already :user_id
  """

  defstruct [:adapter, :env, :name, :user_id]
end

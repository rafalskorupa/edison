# Edison

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.


Postman documentation: [Link](https://documenter.getpostman.com/view/4943667/SWTBdxYq?version=latest)


## Playing around

Simple generated views to modify and manipulate users without accessing console
http://localhost:4000/admin/users

Create your own user - authorization token will be generated automaticaly (just use is as "Authorization" header value)
Todoist key is optional, but required to synchronise with todoist (obviously). 
Without this token only csv synchronisation is available (see Postman docs).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix

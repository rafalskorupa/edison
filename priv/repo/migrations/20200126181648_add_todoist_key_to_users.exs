defmodule Edison.Repo.Migrations.AddTodoistKeyToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add(:todoist_key, :string)
    end
  end
end

defmodule Edison.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :authentication_token, :string

      timestamps()
    end

    create unique_index(:users, [:name])
    create index(:users, [:authentication_token], unique: true)
  end
end

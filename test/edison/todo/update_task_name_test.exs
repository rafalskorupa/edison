defmodule Edison.Todo.Tasks.UpdateTaskNameTest do
  import EdisonFactory

  use Edison.DataCase

  use ExUnit.Case, async: true
  use ExVCR.Mock, adapter: ExVCR.Adapter.Httpc

  alias Edison.Todo.Tasks.UpdateTaskName

  describe "call/1 with todoist" do
    setup :todoist_adapter_vcr
    setup :create_task

    test "when there are tasks synced", %{task: task} do
      use_cassette("update_task/task_updated") do
        assert {:ok, task} = UpdateTaskName.call(task, "new name")

        assert task.name == "new name"
        assert "#{Map.get(task.raw, "id")}" == task.external_id
      end
    end
  end

  defp todoist_adapter_vcr(_ctx) do
    ExVCR.Config.cassette_library_dir("fixture/vcr_cassettes/todoist")

    %{
      task_id: "3651896093",
      user:
        insert(:user,
          todoist_key: System.get_env("TODOIST_API_KEY") || "**"
        ),
      source: "todoist"
    }
  end

  defp create_task(ctx) do
    %{
      task: insert(:task, %{external_id: ctx.task_id, source: ctx.source, user: ctx.user})
    }
  end
end

defmodule Edison.Todo.Tasks.SynchroniseTest do
  import EdisonFactory

  use Edison.DataCase

  use ExUnit.Case, async: true
  use ExVCR.Mock, adapter: ExVCR.Adapter.Httpc

  alias Edison.Todo.Tasks.Synchronise

  describe "call/1 with todoist" do
    setup :todoist_adapter_vcr

    test "when there is no tasks synced yet", %{adapter_name: source, user: user} do
      use_cassette("get_active_tasks/3_tasks") do
        {:ok, result} = Synchronise.synchronise_with_source(user.id, source)

        assert %{task_insert: task_inserted} = result

        Enum.map(task_inserted, fn task ->
          assert task.user_id == user.id
          assert task.source == source
        end)
      end
    end
  end

  setup :todoist_adapter_vcr

  test "when there are tasks synced", %{adapter_name: source, user: user} do
    use_cassette("get_active_tasks/3_tasks_modified") do
      assert {:ok, %{task_insert: tasks}} = Synchronise.synchronise_with_source(user.id, source)

      {:ok, result} = Synchronise.synchronise_with_source(user.id, source)

      task_to_keep = Enum.find(tasks, fn task -> task.name == "1" end)

      assert %{task_delete: [%Edison.Todo.Task{name: "3"} = _deleted_task]} = result
      assert %{task_insert: [%Edison.Todo.Task{name: "4"} = inserted_task]} = result
      assert %{task_update: [%Edison.Todo.Task{name: "2 - updated"} = updated_task]} = result

      expected_tasks_ids =
        [task_to_keep, inserted_task, updated_task] |> Enum.map(fn t -> t.id end) |> Enum.sort()

      tasks_in_database_ids =
        Edison.Todo.list_tasks(user.id) |> Enum.map(fn t -> t.id end) |> Enum.sort()

      assert expected_tasks_ids == tasks_in_database_ids
    end
  end

  def todoist_adapter_vcr(_ctx) do
    ExVCR.Config.cassette_library_dir("fixture/vcr_cassettes/todoist")

    %{
      adapter_name: "todoist",
      user:
        insert(:user,
          todoist_key: System.get_env("TODOIST_API_KEY") || "**"
        )
    }
  end
end

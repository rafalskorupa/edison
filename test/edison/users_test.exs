defmodule Edison.UsersTest do
  use Edison.DataCase

  alias Edison.Users
  alias Edison.Users.User

  import EdisonFactory
  import Mock

  describe "list/users/0" do
    setup do
      %{users: insert_list(2, :user)}
    end

    test "returns all users", %{users: users} do
      assert Users.list_users() == users
    end
  end

  describe "get_user!/1" do
    setup :create_user

    test "returns the user with given id", %{user: user} do
      assert Users.get_user!(user.id) == user
    end
  end

  describe "get_by_authentication_token/1" do
    setup :create_user

    test "returns the user with given authentication_token", %{user: user} do
      assert Users.get_by_authentication_token(user.authentication_token) == user
    end

    test "returns nil if authentication token is invalid" do
      assert Users.get_by_authentication_token("missing_token") == nil
    end
  end

  describe "create_user/1" do
    setup do
      %{
        attributes: %{name: "Nicola Tesla"},
        token: "token"
      }
    end

    test "with valid data creates a user", %{attributes: attributes, token: token} do
      with_mock Edison.Support.TokenGenerator, generate_token: fn -> token end do
        assert {:ok, %User{} = user} = Users.create_user(attributes)

        assert user.authentication_token == token
        assert user.name == attributes.name
      end
    end

    @name_already_taken_errors [
      name: {"has already been taken", [constraint: :unique, constraint_name: "users_name_index"]}
    ]

    test "with non unique name returns error changeset", %{attributes: attributes} do
      insert(:user, attributes)

      assert {:error, %Ecto.Changeset{} = changeset} = Users.create_user(attributes)
      assert changeset.errors == @name_already_taken_errors
    end

    test "with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Users.create_user(%{name: nil})
    end
  end

  describe "update_user/2" do
    setup :create_user

    setup do
      %{attributes: %{name: "new name"}}
    end

    test "with valid data updates the user", %{attributes: attributes, user: user} do
      assert {:ok, %User{} = updated_user} = Users.update_user(user, attributes)
      assert updated_user.name == attributes.name
    end

    test "with invalid data returns error changeset", %{user: user} do
      assert {:error, %Ecto.Changeset{}} = Users.update_user(user, %{name: nil})
      assert user == Users.get_user!(user.id)
    end
  end

  describe "delete_user/1" do
    setup :create_user

    test "deletes the user", %{user: user} do
      assert {:ok, %User{}} = Users.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Users.get_user!(user.id) end
    end
  end

  describe "change_user/1" do
    setup :create_user

    test "returns a user changeset", %{user: user} do
      assert %Ecto.Changeset{} = Users.change_user(user)
    end
  end

  defp create_user(_context) do
    %{user: insert(:user)}
  end
end

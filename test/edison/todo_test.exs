defmodule Edison.TodoTest do
  use Edison.DataCase
  import EdisonFactory

  alias Edison.Todo

  describe "tasks" do
    alias Edison.Todo.Task

    @invalid_attrs %{name: nil}

    def task_fixture(attrs \\ %{}) do
      task = insert(:task, attrs)
      Todo.get_task!(task.id)
    end

    test "list_tasks/1 returns all tasks" do
      task = task_fixture()
      assert Todo.list_tasks(task.user_id) == [task]
    end

    test "get_task!/1 returns the task with given id" do
      task = task_fixture()
      assert Todo.get_task!(task.id) == task
    end

    test "create_task/1 with valid data creates a task" do
      user = insert(:user)
      task_attributes = params_for(:task, user_id: user.id)

      assert {:ok, %Task{} = task} = Todo.create_task(task_attributes)

      assert task.name == task_attributes.name
      assert task.source == task_attributes.source
      assert task.external_id == task_attributes.external_id
      assert task.user_id == user.id
    end

    test "create_task/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Todo.create_task(@invalid_attrs)
    end

    test "update_task/2 with invalid data returns error changeset" do
      task = task_fixture()
      assert {:error, %Ecto.Changeset{}} = Todo.update_task(task, @invalid_attrs)
      assert task == Todo.get_task!(task.id)
    end

    test "delete_task/1 deletes the task" do
      task = task_fixture()
      assert {:ok, %Task{}} = Todo.delete_task(task)
      assert_raise Ecto.NoResultsError, fn -> Todo.get_task!(task.id) end
    end

    test "change_task/1 returns a task changeset" do
      task = task_fixture()
      assert %Ecto.Changeset{} = Todo.change_task(task)
    end
  end
end

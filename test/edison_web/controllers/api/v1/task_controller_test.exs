defmodule EdisonWeb.TaskControllerTest do
  use EdisonWeb.ConnCase
  import EdisonFactory
  import Edison.CollectionHelper, only: [pluck_sorted: 2]
  alias Edison.Todo.Task

  use ExUnit.Case, async: true
  use ExVCR.Mock, adapter: ExVCR.Adapter.Httpc

  @update_attrs %{
    name: "new name"
  }
  @invalid_attrs %{name: nil}

  setup %{conn: conn} = ctx do
    user = Map.get(ctx, :user, insert(:user))

    %{
      conn:
        conn
        |> put_req_header("accept", "application/json")
        |> put_req_header("authorization", user.authentication_token),
      user: user
    }
  end

  describe "GET api/v1/tasks" do
    test "without parameters lists all user tasks", %{conn: conn, user: user} do
      user_tasks = insert_list(3, :task, %{user: user})

      conn = get(conn, Routes.task_path(conn, :index))
      assert tasks = json_response(conn, 200)["data"]
      assert Enum.count(tasks) == Enum.count(user_tasks)

      assert pluck_sorted(tasks, "id") == pluck_sorted(user_tasks, :id)
    end

    test "with params filters user tasks", %{conn: conn, user: user} do
      expected_tasks = [
        insert(:task, %{user: user, name: "Buy bread", source: "todoist"}),
        insert(:task, %{user: user, name: "Sell bread", source: "todoist"})
      ]

      insert(:task, %{user: user, name: "Buy butter", source: "todoist"})
      insert(:task, %{user: user, name: "Buy bread", source: "csv"})
      insert(:task, %{name: "Buy bread", source: "todoist"})

      conn = get(conn, Routes.task_path(conn, :index), %{name: "%bread", source: "todoist"})

      assert tasks = json_response(conn, 200)["data"]
      assert Enum.count(tasks) == Enum.count(expected_tasks)
      assert pluck_sorted(tasks, "id") == pluck_sorted(expected_tasks, :id)
    end

    test "renders errors when filter has invalid value", %{conn: conn} do
      conn = get(conn, Routes.task_path(conn, :index), %{"name" => 2})
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update task" do
    setup :create_task

    test "renders task when data is valid", %{conn: conn, task: %Task{id: id} = task} do
      conn = put(conn, Routes.task_path(conn, :update, task), task: @update_attrs)

      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.task_path(conn, :show, id))

      assert %{
               "id" => id,
               "name" => "new name"
             } = json_response(conn, 200)["data"]
    end
  end

  describe "update task with todoist adapter" do
    setup :todoist_adapter_vcr
    setup :create_cassette_task

    setup %{user: user} do
      %{
        conn:
          build_conn()
          |> put_req_header("accept", "application/json")
          |> put_req_header("authorization", user.authentication_token)
      }
    end

    test "renders task when data is valid", %{conn: conn, task: %Task{id: id} = task} do
      use_cassette("update_task/task_updated") do
        conn = put(conn, Routes.task_path(conn, :update, task), task: @update_attrs)

        assert %{"id" => ^id} = json_response(conn, 200)["data"]

        conn = get(conn, Routes.task_path(conn, :show, id))

        assert %{
                 "id" => id,
                 "name" => "new name"
               } = json_response(conn, 200)["data"]
      end
    end

    test "renders errors when data is invalid", %{conn: conn, task: task} do
      conn = put(conn, Routes.task_path(conn, :update, task), task: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "POST api/v1/todoist/sync" do
    setup :todoist_adapter_vcr

    setup(%{user: user}) do
      %{
        conn:
          build_conn()
          |> put_req_header("accept", "application/json")
          |> put_req_header("authorization", user.authentication_token)
      }
    end

    test "renders sync response", %{conn: conn} do
      use_cassette("get_active_tasks/3_tasks") do
        conn = post(conn, Routes.task_path(conn, :sync, "todoist"))

        assert %{
                 "created" => [
                   %{
                     "name" => "1",
                     "source" => "todoist"
                   },
                   %{
                     "name" => "2 - updated",
                     "source" => "todoist"
                   },
                   %{
                     "name" => "added 5",
                     "source" => "todoist"
                   }
                 ],
                 "deleted" => [],
                 "updated" => []
               } = json_response(conn, 200)["data"]
      end
    end
  end

  describe "POST api/v1/csv/sync" do
    @upload_sync_file %Plug.Upload{
      path: "fixture/csv/sync.csv",
      filename: "sync.cvs"
    }

    setup %{user: user} do
      %{
        tasks: %{
          to_keep: insert(:task, %{user: user, source: "csv", name: "keep", external_id: "1"}),
          to_rename:
            insert(:task, %{user: user, source: "csv", name: "rename", external_id: "2"}),
          to_delete: insert(:task, %{user: user, source: "csv", name: "delete", external_id: "3"})
        }
      }
    end

    test "renders sync response", %{conn: conn, tasks: task} do
      conn = post(conn, Routes.task_path(conn, :sync, "csv"), %{file: @upload_sync_file})

      assert %{
               "created" => [created_task],
               "updated" => [updated_task],
               "deleted" => [deleted_task]
             } = json_response(conn, 200)["data"]

      assert Map.get(created_task, "external_id") == "4"
      assert Map.get(deleted_task, "external_id") == task.to_delete.external_id
      assert Map.get(updated_task, "external_id") == task.to_rename.external_id
      assert Map.get(updated_task, "name") == "new name"
    end
  end

  defp create_task(%{user: user}) do
    %{task: insert(:task, %{user: user})}
  end

  defp create_cassette_task(%{user: user}) do
    %{task: insert(:task, external_id: "3651896093", user: user, source: "todoist")}
  end

  def todoist_adapter_vcr(_ctx) do
    ExVCR.Config.cassette_library_dir("fixture/vcr_cassettes/todoist")

    %{
      adapter_name: "todoist",
      user: insert(:user, todoist_key: System.get_env("TODOIST_API_KEY") || "***")
    }
  end
end

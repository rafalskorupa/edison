defmodule EdisonWeb.Api.V1.SessionControllerTest do
  use EdisonWeb.ConnCase
  import EdisonFactory

  setup do
    user = insert(:user)

    %{
      user: user,
      conn: build_conn() |> put_req_header("authorization", user.authentication_token)
    }
  end

  test "GET api/v1/sessions/current", %{conn: conn, user: user} do
    conn = get(conn, "api/v1/sessions/current")

    assert json_response(conn, 200) == %{
             "data" => %{"id" => user.id, "name" => user.name}
           }
  end

  test "GET api/v1/sessions/current with invalid token" do
    conn =
      build_conn()
      |> put_req_header("authorization", "invalid_token")
      |> get("api/v1/sessions/current")

    assert json_response(conn, 400) == %{"error" => "Authorization  Token invalid"}
  end

  test "GET api/v1/sessions/current with missing token" do
    conn = build_conn() |> get("api/v1/sessions/current")

    assert json_response(conn, 401) == %{"error" => "Authorization Token missing"}
  end
end

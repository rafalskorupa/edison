defmodule Adapters.TodoistTest do
  alias Adapters.Todoist

  use ExUnit.Case, async: true
  use ExVCR.Mock, adapter: ExVCR.Adapter.Httpc

  setup do
    ExVCR.Config.cassette_library_dir("fixture/vcr_cassettes/todoist")

    %{
      token: System.get_env("TODOIST_TEST_TOKEN") || "***"
    }
  end

  describe "get_active_tasks/1" do
    test "with valid token", %{token: token} do
      use_cassette "get_active_tasks" do
        assert returned_tasks = Todoist.get_active_tasks(token)
        Enum.map(returned_tasks, fn task -> assert %Adapters.ExternalTask{} = task end)
      end
    end
  end

  describe "update_task_name/1" do
    setup :active_tasks

    test "with valid payload", %{token: token, first_task: first_task} do
      use_cassette("update_task_name/valid") do
        assert {:ok, updated_task} =
                 Todoist.update_task_name(token, first_task.external_id, "super new name")

        assert %Adapters.ExternalTask{} = updated_task
        assert updated_task.name == "super new name"
      end
    end

    test "with invalid external id", %{token: token} do
      use_cassette("update_task_name/invalid_external_id") do
        assert {:error, anything} = Todoist.update_task_name(token, "2651089392", "updated_name")
      end
    end
  end

  defp active_tasks(context) do
    use_cassette "get_active_tasks" do
      tasks = Todoist.get_active_tasks(context.token)

      %{
        tasks: tasks,
        first_task: tasks |> Enum.at(0)
      }
    end
  end
end

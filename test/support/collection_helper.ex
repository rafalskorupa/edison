defmodule Edison.CollectionHelper do
  @moduledoc """
  A set of helpers used for mapping collections
  """
  def pluck_sorted(collection, property) do
    collection
    |> Enum.map(fn entry -> Map.get(entry, property) end)
    |> Enum.sort()
  end
end
